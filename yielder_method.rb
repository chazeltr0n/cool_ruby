def do_something(&some_block)
  loop do
    puts "running"
    result = yield
    puts "epoch time + 100 = #{result + 100}"
    sleep 3
  end
end

do_something do
  t = Time.now
  puts "epoch time is #{t.to_i}"
  t.to_i
end
